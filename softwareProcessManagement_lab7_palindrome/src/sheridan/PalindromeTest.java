package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
		
	}

	@Test
	public void testIsPalindrome() {
		assertTrue("Invalid value", Palindrome.isPalindrome("Anna"));
	}

	@Test
	public void testNegativeIsPalindrome() {
		assertFalse("Invalid value", Palindrome.isPalindrome("Air"));
	}
	
	@Test
	public void testBoundaryIn_IsPalindrome() {
		assertTrue("Invalid value", Palindrome.isPalindrome("A"));
	}
	
	@Test
	public void testBoundaryOut_IsPalindrome() {
		assertFalse("Invalid value", Palindrome.isPalindrome("Racer car"));
	}
	
}
