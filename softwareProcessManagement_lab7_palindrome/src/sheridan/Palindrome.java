package sheridan;

public class Palindrome {

	public static boolean isPalindrome(String input) {
		
		input = input.replaceAll(" ", "").toUpperCase();
		for (int i = 0, j = input.length() - 1; i < j; i++, j--) {
			if (input.charAt(i) != input.charAt(j)) {
				return false;
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		
		System.out.println("Is anna a palindrome? " + isPalindrome("anna"));
	}
}
